package az.techlab.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/home")
public class HomeController {

    @GetMapping(path = "guest")
    public String helloGuest() {
        return "Hello Guest!";
    }

    @GetMapping(path = "user")
    public String helloUser(Principal principal) {
        return String.format("Hello, %s", principal.getName());
    }
}
